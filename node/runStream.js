require('dotenv').config();
const shellExec = require('shell-exec');
var cron = require('node-cron');

var Twitter = require('twitter');
var fs = require('fs');
console.log("script run Stream");

var date = new Date();
var month = date.getUTCMonth();
var day = date.getUTCDay();
var currentYear = date.getUTCFullYear();
var dateString = month + "_" + day + "_" + date.getUTCHours() + "h" + date.getUTCMinutes();

var hashtagsFiles = './data/hashtagsFiles/';
var jsonFileName = `hashtags_${dateString}.json`;
var fullPath = hashtagsFiles + currentYear + "/" + jsonFileName;

var client = new Twitter({
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token_key: process.env.access_token_key,
    access_token_secret: process.env.access_token_secret
});

let filters = ["ajax","apache","html","j2ee","java","php","rubyonrails","nodejs", "vuejs","react","angular","javascript","js"];

for (let i = 0; i < 6; i++){
    let numberOfDayBefore = i+1;
    let dateBefore = new Date(date.getTime() - (numberOfDayBefore * 24 * 60 * 60 * 1000));
    let dateBeforeMinusOne = new Date(date.getTime() - ((numberOfDayBefore-1) * 24 * 60 * 60 * 1000));
    

    let yearMonthDayString = dateBefore.toISOString().split("T")[0];
    let yearMonthDayStringMinusOne =  dateBeforeMinusOne.toISOString().split("T")[0];

    callTwitterApi(yearMonthDayString, yearMonthDayStringMinusOne);

    cron.schedule('*/* * 7 * *', () => {
        callTwitterApi();
    });
}

function callTwitterApi(yearMonthDayString, yearMonthDayStringMinusOne) {
    // console.log('Start importing tweet in for:'+yearMonthDayString+" _ "+yearMonthDayStringMinusOne);

    const params = {
        q: '#'+filters.join(' OR #'),
        // q: '#lol',
        count: 1,
        until: yearMonthDayStringMinusOne,
        since: yearMonthDayString
    };
    
    
    let finalString = "";

    client.get('search/tweets', params, function (error, tweets, response) {
        console.log("Got http response", response.statusCode);
        console.log("Error :", error);
        if (!error) {
            console.log("Got " + tweets.statuses.length + " tweets for : "+yearMonthDayString+"_"+yearMonthDayStringMinusOne);
            if (tweets.statuses.length > 0) {
                tweets.statuses.forEach((tweet, index) => {

                    let hashtags = tweet.entities.hashtags;

                    hashtags.forEach((hastag, index) => {
                        if(filters.indexOf(hastag.text.toLowerCase())>=0){
                            finalString += `
{"index": { "_index": "hashtag", "_type": "_doc" }}
{"label" : "${hastag.text.toLowerCase()}","retweet_count": ${tweet.retweet_count},"favorite_count": ${tweet.favorite_count},"created_at": "${new Date(tweet.created_at).toISOString()}","device" : "${tweet.user.location}","lang" : "${tweet.lang}"}`
                        
                        }
                    });
                })

                if (!fs.existsSync(hashtagsFiles + currentYear)) fs.mkdirSync(hashtagsFiles + currentYear);

                fs.writeFile(fullPath, finalString + "\n", { 'flag': 'a' }, function (err) {
                    if (err) {
                        return console.error(err);
                    } else {
                        console.log("Writing " + jsonFileName);
                    }
                });

                console.log("Start importing to elasticsearch");
                shellExec(`curl --user elastic:changeme -H "Content-Type: application/json" -XPOST "localhost:9200/_bulk?pretty&refresh" --data-binary "@${fullPath.substr(2)}"`)
                    // .then(console.log)
                    .catch(console.log);
            }
        }
    });
}