# 13
GET /my_blogs/_search
{
"query": {
    "bool": {
          "must": {
              "match": { "content":  "performance optimizations improvements"}
          },
          "should": {
              "match_phrase": { "content":  "performance optimizations improvements"}
            }
       }
 }
}




# 12
GET /my_blogs/_search
{
  "query": {
    "match": {
      "content":{
       "query": "performance optimizations improvements",
	      "minimum_should_match": 2
        }
    }
  }
}


# 11
GET /my_blogs/_search
{
  "query": {
    "match": {
      "content":{
       "query": "performance optimizations improvements",
	      "operator": "or"
        }
    }
  }
}


# 10
GET /my_blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match_phrase": {
            "content": {
              "query": "search analytics",
              "slop": 1
            }
          }
        }
      ]
    }
  },
  "aggs": {
    "top_content": {
      "top_hits": {
        "size": 3
      }
    }
  },
  "size": 3
}


# 9
GET /my_blogs/_search
{
  "query":{
    "bool": {
      "must": [
        {
          "match_phrase": {
            "content": "search analytics"
          }
        }
      ]
    }
  },
  "aggs": {
    "top_content": {
      "top_hits": {
        "size": 3
      }
    }
  },
  "size": 3
}


# 8.3
GET /my_blogs/_search
{
  "query": {
    "match": {
      "content":{
       "query": "search content",
      "operator": "and"
        }
    }
  }
}


# 8.2
GET /my_blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "content": "search analytics"
          }
        }
      ]
    }
  }
}


# 8.1
GET /my_blogs/_search
{
"query": {
    "bool": {
      "must": {
          "match": {
                "content": "search"
        }
      }
      }
  }
}


# 7
GET /my_blogs/_search
{
  "query": {
    "match": {
      "title":{
       "query": "elastic stack",
      "operator": "and"
        }
    }
  }
}


# 6
GET /my_blogs/_search
{
"query": {
    "bool": {
      "must": {
          "match": {
                "title": "elastic stack"
        }
      }
      }
  }
}


# 5
GET /my_blogs/_search
{
"query": {
    "bool": {
      "must": {
          "match": {
                "title": "elastic"
        }
      }
      }
  }
}

# 4
GET /my_blogs/_search
{
    "query": {
     "range": {
            "date": {
              "gte": "2017-05-01T00:00:00.000Z",
              "lte": "2017-05-31T00:00:00.000Z"
            }
          }
    }
}

# 3
GET /my_blogs/_search
{
  "size": 100,
  "query":
   {
    "match_all": {}
   }
}

# 2
GET /my_blogs/_search
{
  "query":{
    "match_all": {} 
  }
}

DELETE /my_blogs













